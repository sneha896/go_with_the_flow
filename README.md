In typesetting, a “river” is a string of spaces formed by gaps between words that extends down sev-
eral lines of text. For instance.
Celebrated river authority Flo Ng wants her new book on rivers of the world to include the longest
typographic rivers possible. She plans to set the text in a mono-spaced font (all letters and spaces have
equal width) in a left-aligned column of some fixed width, with exactly one space separating words on
each line (the text is not aligned on the right). For Flo, a “river” is defined as a sequence of spaces lying
in consecutive lines in which the position of each space in the sequence (except the first) differs by at
most 1 from the position of the space in the line above it. Trailing white space cannot appear in a river.
Words must be packed as tightly as possible on lines; no words may be split across lines. The line width
used must be at least as long as the longest word in the text.
